import { html, TemplateResult } from 'lit';
import '../src/stativa-image.js';

export default {
  title: 'StativaImage',
  component: 'stativa-image',
  argTypes: {
    header: { control: 'text' },
    counter: { control: 'number' },
    textColor: { control: 'color' },
  },
};

interface Story<T> {
  (args: T): TemplateResult;
  args?: Partial<T>;
  argTypes?: Record<string, unknown>;
}

interface ArgTypes {
  width?: number;
  height?: number;
  alt?: string;
  src?: string;
  slot?: TemplateResult;
}

const Template: Story<ArgTypes> = ({
  width = 100,
  height = 100,
  src= 'https://upload.wikimedia.org/wikipedia/commons/7/76/Turkish_Angora_in_Ankara_Zoo_%28AO%C3%87%29.JPG',
  alt= 'test',
  slot,
}: ArgTypes) => html`
  <stativa-image
    .width=${width}
    .height=${height}
    .src=${src}
    .alt=${alt}
  >
    ${slot}
  </stativa-image>
`;

export const Regular = Template.bind({});

export const CustomHeader = Template.bind({});
CustomHeader.args = {
  // header: 'My header',
};

export const CustomCounter = Template.bind({});
CustomCounter.args = {
  width: 100,
  height: 100,
  src: 'https://upload.wikimedia.org/wikipedia/commons/7/76/Turkish_Angora_in_Ankara_Zoo_%28AO%C3%87%29.JPG',
  alt: 'test'
};

export const SlottedContent = Template.bind({});
SlottedContent.args = {
  slot: html`<p>Slotted content</p>`,
};
SlottedContent.argTypes = {
  slot: { table: { disable: true } },
};

import { html, css, LitElement } from 'lit';
import { property } from 'lit/decorators.js';

export class StativaImage extends LitElement {
  static styles = css`
    :host {
      display: block;
      color: var(--stativa-image-text-color, #000);
    }

    .image {
      width: 100%;
      height: auto;
    }

    img {
      width: 100%;
      height: 100%;
    }
  `;

  @property({ type: Number }) width = 0;

  @property({ type: Number }) height = 0;

  @property({ type: String }) src = "url";

  @property({ type: String }) alt = "alr";

  @property({ type: String }) title = "";

  @property({ type: String }) description = "";

  render() {
    return html`
      <div class="image">
        <h1>${this.title}</h1>
        <img src=${this.src} alt=${this.alt} width=${this.width} height=${this.height} />
        <p>${this.description}</p>
      </div>
    `;
  }
}
